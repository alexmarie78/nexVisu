import sys
import unittest

from PyQt5.QtWidgets import QApplication
from silx.gui.colors import Colormap
from pytestqt import qtbot

from nexvisu.detectors.xpad.initialDataTab.flatfieldTab.flatfieldGroup import FlatfieldGroup, get_scan_number
from nexvisu.detectors.xpad.initialDataTab.xpadContext import DataContext


class TestXpadContext(unittest.TestCase):
    def setUp(self):
        self.app = QApplication.instance()
        if not self.app:
            self.app = QApplication(sys.argv)
        self.parent = DataContext(self.app)
        #self.widget = FlatfieldGroup(self.parent, self.app)

    def test_get_scan_number(self):
        self.assertIsInstance(get_scan_number("0"), int)
        with self.assertRaises(AttributeError):
            get_scan_number(0)
        self.assertEqual(get_scan_number("module_scan_777_nexus.nxs"), 777)
        self.assertEqual(get_scan_number("module_scan_777_0001.nxs"), 777)

    def test_flatfield_group(self):
        self.assertIsInstance(self.parent.flatfield_tab, FlatfieldGroup)
        self.assertEqual(self.parent.flatfield_tab.colormap, Colormap("viridis", normalization='log'))


if __name__ == '__main__':
    unittest.main()
