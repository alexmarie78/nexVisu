import unittest
from nexvisu.utils.cacheFunctions import memoisation


class TestCacheFunctions(unittest.TestCase):
    def test_memoisation(self):
        self.assertEqual(memoisation({}), ())
        self.assertEqual(memoisation({}, True), (True,))
        self.assertIsInstance(memoisation({}), tuple)
        self.assertEqual(memoisation({"test": 20}, True), (20, True))
        self.assertEqual(memoisation({"test1": 20, "test2": 45}, False), (20, 45, False))
        self.assertEqual(memoisation({"test1": 20, "test2": -45}, False), (20, -45, False))
        self.assertEqual(memoisation({"test1": -0.5, "test2": -2.2}, False), (-0.5, -2.2, False))


if __name__ == '__main__':
    unittest.main()
