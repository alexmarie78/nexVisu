import sys
import unittest

from PyQt5.QtWidgets import QLineEdit, QApplication

from nexvisu.detectors.xpad.initialDataTab.contextualDataTab.directBeamWidget import DirectBeamWidget, read_calibration, is_number


class TestContextualData(unittest.TestCase):
    def setUp(self):
        self.app = QApplication.instance()
        if not self.app:
            self.app = QApplication(sys.argv)

    def test_direct_beam_widget(self):
        dbw = DirectBeamWidget(None)
        dbw.show()
        self.assertIsInstance(dbw, DirectBeamWidget)
        self.assertEqual(len(dbw.input_widgets), 4)
        for input_list in dbw.input_lists.values():
            self.assertIsInstance(input_list, list)
            self.assertIsInstance(input_list[0], QLineEdit)
        self.assertEqual(list(dbw.input_lists.keys()), ['0', '1', '2', '3'])

        lines = dbw.input_widgets[0].layout().count()

        dbw.add_row()
        for widget in dbw.input_widgets:
            if lines < 5:
                self.assertEqual(lines + 1, widget.layout().count())
                self.assertEqual(lines + 1, dbw.input_number())
            else:
                self.assertEqual(lines, widget.layout().count())
                self.assertEqual(lines, dbw.input_number())

        lines = dbw.input_widgets[0].layout().count()
        dbw.remove_row()
        for widget in dbw.input_widgets:
            if lines > 0:
                self.assertEqual(lines - 1, widget.layout().count())
                self.assertEqual(lines - 1, dbw.input_number())
            else:
                self.assertEqual(lines, widget.layout().count())
                self.assertEqual(lines, dbw.input_number())

        for i in range(5):
            dbw.add_row()
        self.assertEqual(dbw.input_widgets[0].layout().count(), 5)
        self.assertEqual(dbw.input_number(), 5)
        for i in range(5):
            dbw.remove_row()
        self.assertEqual(dbw.input_widgets[0].layout().count(), 1)
        self.assertEqual(dbw.input_number(), 1)

    def test_get_contextual_data(self):
        dbw = DirectBeamWidget(None)
        dbw.show()
        result = dbw.get_contextual_data()
        self.assertIsInstance(result, dict)
        for input_list in result.values():
            self.assertIsInstance(input_list, list)
            for value in input_list:
                self.assertIsInstance(value, float or None)
        self.assertEqual(list(result.keys()), ['x', 'y', 'delta_position', 'gamma_position', 'distance'])

    def test_read_calibration(self):
        result = read_calibration()
        if result is not None:
            self.assertIsInstance(result, dict)
            self.assertEqual(list(result.keys()), ['x', 'y', 'delta_position', 'gamma_position', 'distance'])
            for input_list in result.values():
                self.assertIsInstance(input_list, list)
                for value in input_list:
                    self.assertIsInstance(value, float or None)

    def test_is_a_number(self):
        self.assertEqual(is_number("5"), True)
        self.assertEqual(is_number(5), True)
        self.assertEqual(is_number("az"), False)
        self.assertEqual(is_number("%%2"), False)


if __name__ == '__main__':
    unittest.main()
